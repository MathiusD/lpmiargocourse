// Player created by Féry Mathieu and Mathilde Ballouhey
// Player represent client of player during session.
// This structure is used by server for manage session.

package main

import "net"

type Player struct {
	addrRelated      net.Addr    // Address related to this player
	lastMsg          string      // Last message send by this player
	outputChan       chan string // Chan used for send data to this player
	color            int         // Color selected by player
	customized       bool        // Flag indicate if this player are ended customization
	readyToCountdown bool        // Flag indicate if this player are ready for launch countdown phase
	ended            bool        // Flag indicate if this player are end this run
	endAt            float64     // Flag indicate time of run for this player
}

// Method for reset player before start another run
func (player *Player) Reset() {
	player.ended = false
	player.readyToCountdown = false
}
