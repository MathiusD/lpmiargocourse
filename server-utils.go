// Server Utils created by Féry Mathieu and Mathilde Ballouhey
// Server Utils contains many basic method for simplify usage of server

package main

import (
	"io/ioutil"
	"log"
	"net"
	"net/http"
)

const uriApi = "https://api.ipify.org?format=text"

// Method for get public address related to this host
// Based on this gist : https://gist.github.com/ankanch/8c8ec5aaf374039504946e7e2b2cdf7f
func (server *Server) getPublicAddr() {
	log.Println("Getting public IP address")
	resp, err := http.Get(uriApi)
	if err != nil {
		log.Print("Error as occured in communication with api.\nBase Error : ", err)
	} else {
		defer resp.Body.Close()
		ip, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Print("Error as occured in read return of api.\nBase Error : ", err)
		} else {
			log.Print("Public address are found : ", string(ip))
			server.host.publicAddr = net.ParseIP(string(ip))
		}
	}
}

// Method for manage all incomming connection to server
func (server Server) Accept() {
	for {
		conn, _ := server.listener.Accept()
		server.connChan <- conn
	}
}

// Method for manage connection with player
func (server Server) handleConnection(conn net.Conn) {
	log.Print("Connection initiate with ", conn.RemoteAddr())

	inputData := make(chan string, defaultChanSize)
	go server.host.readFrom(conn, inputData)
	outputChan := make(chan string, defaultChanSize)

	for {
		select {
		case output := <-outputChan:
			server.host.sendTo(conn, output)
		case input := <-inputData:
			server.playerChan <- Player{
				addrRelated: conn.RemoteAddr(),
				lastMsg:     input,
				outputChan:  outputChan,
			}
		}
	}
}

// Method for send msg to all players
func (server Server) SendAll(msg string) {
	log.Print("Bekipan launch !")
	for _, player := range server.players {
		if player.outputChan != nil {
			player.outputChan <- msg
		}
	}
	log.Print("Bekipan send.")
}

// Method for send lastMsg of writer to other players
func (server Server) SendAllExceptWriter(writer Player) {
	log.Print("Exploring Team deployed !")
	for _, player := range server.players {
		if player.outputChan != nil && player.addrRelated != writer.addrRelated {
			player.outputChan <- writer.lastMsg
		}
	}
	log.Print("Exploring Team returning to camp.")
}
