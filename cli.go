// CLI file created by Féry Mathieu and Mathilde Ballouhey
// Function for manage arguments given by CLI

package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
)

// Const if argument call with missing or incorrect argument
const wrongUsage = "Wrong usage with this argument, help related :"

var (
	withServerArgs = []string{
		"--withServer",
		"--serv",
		"-s",
	} // Args for specify this instance must launch server attached
	withServerOnlyArgs = []string{
		"--withServerOnly",
		"--servOnly",
		"-so",
	} // Args for specify this instance must launch only server attached and doesn't launch game
	hostArgs = []string{
		"--host",
		"-H",
	} // Args for specify host of target host or server if server attached
	portArgs = []string{
		"--port",
		"-p",
	} // Args for specify port of target host or server if server attached
	helpArgs = []string{
		"--help",
		"-h",
	} // Args for show help and exit
	withServerHelp = fmt.Sprint(
		withServerArgs,
		" for specify this instance must launch server attached.",
	) // Help related to Server Argument
	withServerOnlyHelp = fmt.Sprint(
		withServerOnlyArgs,
		" for specify this instance must launch only server attached and doesn't launch game.",
	) // Help related to ServerOnly Argument
	hostHelp = fmt.Sprint(
		hostArgs,
		" <host[:port]> for specify host of target host or server if server attached. Example : `",
		hostArgs[rand.Intn(len(hostArgs))],
		" localhost:8080`.",
	) // Help related to Host Argument
	portHelp = fmt.Sprint(
		portArgs,
		" <port> for specify port of target host or server if server attached. Example : `",
		portArgs[rand.Intn(len(portArgs))],
		" 8080`.",
	) // Help related to Port Argument
	helpHelp = fmt.Sprint(
		helpArgs,
		" for display this help and quit.",
	) // Help related to Port Argument
	allHelp = fmt.Sprint(
		"Help :\n",
		withServerHelp, "\n",
		withServerOnlyHelp, "\n",
		hostHelp, "\n",
		portHelp, "\n",
		helpHelp, "\n",
	) // Global Help of this app
)

// Function for extract port from CLI args and return port found
// if port isn't found defaultPort are returned and err related to extraction
func extractPortFromArg(portArg string, defaultPort int) (port int, err error) {
	extractedPort, err := strconv.Atoi(portArg)
	if err != nil {
		log.Printf("Port unknow (%s) using default port instead %d", portArg, defaultPort)
		return defaultPort, err
	} else {
		return extractedPort, nil
	}
}

// Function for extract host from CLI args and return host with port found
// if port or host isn't found default value are used
func extractHost(hostArg string, defaultPort int) (host string, port int) {
	hashHost := strings.Split(hostArg, ":")
	portExtracted := false
	var err error
	if len(hashHost) > 1 {
		port, err = extractPortFromArg(hashHost[len(hashHost)-1], defaultPort)
		if err == nil {
			portExtracted = true
		}
	}
	host = hostArg
	if portExtracted {
		host = strings.Join(hashHost[:len(hashHost)-1], ":")
	}
	return host, port
}

// Function for extract informations of this execution
// and return informations extracted or instead default value
func processArgs() (targetHost string, targetPort int, launchServ bool, startClient bool) {
	pgrmArgs := os.Args[1:]
	targetHost = defaultHost
	targetPort = defaultPort
	launchServ = false
	startClient = true
	log.Print("With args : ", pgrmArgs)
	for index, pgrmArg := range pgrmArgs {
		if strings.Contains(pgrmArg, "-") {
			if FindFirstString(pgrmArg, withServerArgs) != -1 || FindFirstString(pgrmArg, withServerOnlyArgs) != -1 {
				launchServ = true
			}
			if FindFirstString(pgrmArg, hostArgs) != -1 {
				if len(pgrmArgs) >= index+2 {
					targetHost, targetPort = extractHost(pgrmArgs[index+1], targetPort)
				} else {
					fmt.Println(
						wrongUsage, "\n",
						hostHelp,
					)
					os.Exit(1)
				}
			}
			if FindFirstString(pgrmArg, withServerOnlyArgs) != -1 {
				startClient = false
			}
			if FindFirstString(pgrmArg, portArgs) != -1 {
				if len(pgrmArgs) >= index+2 {
					targetPort, _ = extractPortFromArg(pgrmArgs[index+1], targetPort)
				} else {
					fmt.Println(
						wrongUsage, "\n",
						portHelp,
					)
					os.Exit(1)
				}
			}
			if FindFirstString(pgrmArg, helpArgs) != -1 {
				fmt.Print(allHelp)
				os.Exit(0)
			}
		}
	}
	return targetHost, targetPort, launchServ, startClient
}
