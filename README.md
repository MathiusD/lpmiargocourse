# prog-rep-MiAR-2020-2021

Projet de programmation répartie, licence pro MiAR, année 2020-2021, code de base de l'application.

## Launch App

* For compile app :

> ```bash
> go build
> make compile
> ```

* For show help :

> ```bash
> #If app are compiled
> ./course -h
> make start_help
> #If app aren't compiled
> go run . -h
> make help
> ```

* For launch app only (Default app try to connect with localhost:8080):

> ```bash
> #If app are compiled
> ./course
> make start_client
> #If app aren't compiled
> go run .
> make
> make launch_client
> ```

* For launch app only with specific server:

> ```bash
> #If app are compiled
> ./course -H localhost:8080
> ./course --host localhost:8080
> make start_client_with_host
> #If app aren't compiled
> go run . -H localhost:8080
> go run . --host localhost:8080
> make launch_client_with_host
> ```

* For launch app only with local server at specific port:

> ```bash
> #If app are compiled
> ./course -p 8080
> ./course --port 8080
> #If app aren't compiled
> go run . -p 8080
> go run . --port 8080
> ```

* For launch app with server attached (Default server try launch at :8080 and increment if port is'nt free):

> ```bash
> #If app are compiled
> ./course -s
> ./course --serv
> ./course --withServer
> make start_serv
> #If app aren't compiled
> go run . -s
> go run . --serv
> go run . --withServer
> make launch_serv
> ```

* For launch app with server attached at specific port (Default server try launch at port specificed and increment if port is'nt free):

> ```bash
> #If app are compiled
> ./course -s -p 8080
> ./course --serv -p 8080
> ./course --withServer -p 8080
> ./course -s --port 8080
> ./course --serv --port 8080
> ./course --withServer --port 8080
> ./course -s -H :8080
> ./course --serv -H :8080
> ./course --withServer -H :8080
> ./course -s --host :8080
> ./course --serv --host :8080
> ./course --withServer --host :8080
> #If app aren't compiled
> go run . -s -p 8080
> go run . --serv -p 8080
> go run . --withServer -p 8080
> go run . -s --port 8080
> go run . --serv --port 8080
> go run . --withServer --port 8080
> go run . -s -H :8080
> go run . --serv -H :8080
> go run . --withServer -H :8080
> go run . -s --host :8080
> go run . --serv --host :8080
> go run . --withServer --host :8080
> ```

* For launch server only:

> ```bash
> #If app are compiled
> ./course -so
> ./course --servOnly
> ./course --withServerOnly
> make start_serv_only
> #If app aren't compiled
> go run . -so
> go run . --servOnly
> go run . --withServerOnly
> make launch_serv_only
> ```

* For launch server onlyat specific port (Default server try launch at port specificed and increment if port is'nt free):

> ```bash
> #If app are compiled
> ./course -so -p 8080
> ./course --servOnly -p 8080
> ./course --withServerOnly -p 8080
> ./course -so --port 8080
> ./course --servOnly --port 8080
> ./course --withServerOnly --port 8080
> ./course -so -H :8080
> ./course --servOnly -H :8080
> ./course --withServerOnly -H :8080
> ./course -so --host :8080
> ./course --servOnly --host :8080
> ./course --withServerOnly --host :8080
> #If app aren't compiled
> go run . -so -p 8080
> go run . --servOnly -p 8080
> go run . --withServerOnly -p 8080
> go run . -so --port 8080
> go run . --servOnly --port 8080
> go run . --withServerOnly --port 8080
> go run . -so -H :8080
> go run . --servOnly -H :8080
> go run . --withServerOnly -H :8080
> go run . -so --host :8080
> go run . --servOnly --host :8080
> go run . --withServerOnly --host :8080
> ```
