default_target: launch_client

compile:
	@go build

start_serv:
	@./course -s

start_serv_only:
	@./course -so

start_client:
	@./course

start_client_with_host:
	@./course -H localhost:8080

start_help:
	@./course -h

launch_client: compile start_client

launch_client_with_host: compile start_client_with_host

launch_serv: compile start_serv

launch_serv_only: compile start_serv_only

help: compile start_help