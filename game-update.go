/*
//  Implementation of the Update method for the Game structure
//  This method is called once at every frame (60 frames per second)
//  by ebiten, juste before calling the Draw method (game-draw.go).
//  Provided with a few utilitary methods:
//    - CheckArrival
//    - ChooseRunners
//    - HandleLaunchRun
//    - HandleResults
//    - HandleWelcomeScreen
//    - Reset
//    - UpdateAnimation
//    - UpdateRunners
//  Update of game are editing in following methods :
//    - HandleWelcomeScreen
//    - ChooseRunners
//    - HandleLaunchRun
//    - UpdateRunners
//    - CheckArrival
//    - Reset
*/

package main

import (
	"fmt"
	"log"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

// HandleWelcomeScreen waits for the player to push SPACE in order to
// start the game
func (g *Game) HandleWelcomeScreen() bool {
	// Update Method for waiting input and if key are pressed
	// Game notify GameClient by gameClientChan for exchange with
	// server
	// Finally if 4 players are connected to server, index of runner is affected
	// and request of game are sending user can leave Welcome Screen
	updated := false
	if !g.startingGame {
		g.startingGame = inpututil.IsKeyJustPressed(ebiten.KeySpace)
		updated = true
	}
	if g.startingGame && updated {
		log.Print("Connecting...")
		g.gameClientChan <- requestOfConnect
	}
	return g.playerNb == 4 && g.startingGame && g.indexRunner != -1
}

// ChooseRunners loops over all the runners to check which sprite each
// of them selected
func (g *Game) ChooseRunners() (done bool) {
	for i := range g.runners {
		// Update index of runner use Manual Choose for using
		// runner defined by server
		if i == g.indexRunner {
			g.runners[i].ManualChoose()
		} else {
			// Update method for call Network Choose instead RandomChoose
			g.runners[i].NetworkChoose()
		}
	}
	return g.allCharSelected
}

// HandleLaunchRun countdowns to the start of a run
func (g *Game) HandleLaunchRun() bool {
	// Update Method for notify server if game are ready to countdown
	// After that if launchStep if greater than 5 countdown is ended
	// Note : CountDown is now managed by server and not by game.
	if !g.readyForCountdown {
		log.Print("Ready for sprint !")
		g.readyForCountdown = true
		g.gameClientChan <- fmt.Sprint(
			sendServerPrefix,
			defaultDelimInsideMsg,
			g.indexRunner,
			defaultDelimInsideMsg,
			launchReadyFlag,
		)
	}
	if g.launchStep >= 5 {
		g.launchStep = 0
		g.f.chrono = time.Now()
		return true
	}
	return false
}

// UpdateRunners loops over all the runners to update each of them
func (g *Game) UpdateRunners() {
	for i := range g.runners {
		// Update index of runner use Manual Update for using
		// runner defined by server
		if i == g.indexRunner {
			g.runners[i].ManualUpdate()
		} else {
			// Update method for call Network Update instead RandomChoose
			g.runners[i].NetworkUpdate()
		}
	}
}

// CheckArrival loops over all the runners to check which ones are arrived
func (g *Game) CheckArrival() (finished bool) {
	// Update Method for only check Arrival and return runEnded flag
	// This flag are updated by GameClient attached
	for i := range g.runners {
		g.runners[i].CheckArrival(&g.f)
	}
	return g.runEnded
}

// Reset resets all the runners and the field in order to start a new run
func (g *Game) Reset() {
	// Add reset for new flag for make game in stable state
	g.readyForCountdown = false
	g.runEnded = false
	g.launchStep = -1
	for i := range g.runners {
		g.runners[i].Reset(&g.f)
	}
	g.f.Reset()
}

// UpdateAnimation loops over all the runners to update their sprite
func (g *Game) UpdateAnimation() {
	for i := range g.runners {
		g.runners[i].UpdateAnimation(g.runnerImage)
	}
}

// HandleResults computes the resuls of a run and prepare them for
// being displayed
func (g *Game) HandleResults() bool {
	if time.Since(g.f.chrono).Milliseconds() > 1000 || inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		g.resultStep++
		g.f.chrono = time.Now()
	}
	if g.resultStep >= 4 && inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		g.resultStep = 0
		return true
	}
	return false
}

// Update is the main update function of the game. It is called by ebiten
// at each frame (60 times per second) just before calling Draw (game-draw.go)
// Depending of the current state of the game it calls the above utilitary
// function and then it may update the state of the game
func (g *Game) Update() error {
	switch g.state {
	case StateWelcomeScreen:
		done := g.HandleWelcomeScreen()
		if done {
			g.state++
		}
	case StateChooseRunner:
		done := g.ChooseRunners()
		if done {
			g.UpdateAnimation()
			g.state++
		}
	case StateLaunchRun:
		done := g.HandleLaunchRun()
		if done {
			g.state++
		}
	case StateRun:
		g.UpdateRunners()
		finished := g.CheckArrival()
		g.UpdateAnimation()
		if finished {
			g.state++
		}
	case StateResult:
		done := g.HandleResults()
		if done {
			g.Reset()
			g.state = StateLaunchRun
		}
	}
	return nil
}
