/*
//  Implementation of the Draw method for the Game structure
//  This method is called once at every frame (60 frames per second)
//  by ebiten, juste after calling the Update method (game-update.go)
//  Provided with a few utilitary methods:
//    - DrawLaunch
//    - DrawResult
//    - DrawRun
//    - DrawSelectScreen
//    - DrawWelcomeScreen
//  Draw of game are editing in following methods :
//    - DrawWelcomeScreen
//    - DrawSelectScreen
//    - DrawLaunch
*/

package main

import (
	"fmt"
	"image"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

// DrawWelcomeScreen displays the title screen in the game window
func (g *Game) DrawWelcomeScreen(screen *ebiten.Image) {
	ebitenutil.DebugPrintAt(
		screen,
		fmt.Sprint("Track & Field: LP MiAR 2020-2021 Edition"),
		screenWidth/2-120,
		screenHeight/2-20,
	)
	msg := fmt.Sprint("Press SPACE to play")
	width := screenWidth/2 - 60
	// Adding show of number of players connected and configuration phase bellow
	if g.startingGame {
		baseMsg := fmt.Sprintf("%d are connected to %s.", g.playerNb, g.hostRelated.addrRelated)
		if g.indexRunner == -1 && g.playerNb == 4 {
			baseMsg = "Configuring game."
		}
		msg = fmt.Sprintf("%s Please wait...", baseMsg)
		width = screenWidth/2 - 120
	}
	ebitenutil.DebugPrintAt(
		screen,
		msg,
		width,
		screenHeight/2+10,
	)
}

// DrawSelectScreen displays the runner selection screen in the game window
func (g *Game) DrawSelectScreen(screen *ebiten.Image) {
	// Adding number of runner are confirmed inside customization phase bellow
	runnerConfirmed := 0
	for _, runner := range g.runners {
		if runner.ChooseCheck() {
			runnerConfirmed += 1
		}
	}

	confirmedSentence := ""
	if runnerConfirmed > 0 {
		confirmedSentence = fmt.Sprintf(".\n%d Runners are Confirmed.", runnerConfirmed)
	}

	ebitenutil.DebugPrintAt(
		screen,
		fmt.Sprint("Select your player", confirmedSentence),
		screenWidth/2-60,
		20,
	)

	xStep := (screenWidth - 100) / 8
	xPadding := (xStep - 32) / 2
	yPos := (screenHeight - 32) / 2
	for i := 0; i < 8; i++ {
		options := &ebiten.DrawImageOptions{}
		xPos := 50 + i*xStep + xPadding
		options.GeoM.Translate(float64(xPos), float64(yPos))
		screen.DrawImage(g.runnerImage.SubImage(image.Rect(0, i*32, 32, i*32+32)).(*ebiten.Image), options)
	}
	for i := range g.runners {
		g.runners[i].DrawSelection(screen, xStep, i)
	}
}

// DrawLaunch displays the countdown before a run in the game window
func (g *Game) DrawLaunch(screen *ebiten.Image) {
	// Method display countdown if countdown are initialized,
	// in other case display warning of synchronization
	var msg string
	if g.launchStep > -1 {
		msg = fmt.Sprint(5 - g.launchStep)
	} else {
		msg = "Waiting others players"
	}
	ebitenutil.DebugPrintAt(screen, msg, screenWidth/2-10, 10)
}

// DrawRun displays the current state of the run in the game window
func (g *Game) DrawRun(screen *ebiten.Image, drawChrono bool) {
	g.f.Draw(screen, drawChrono)
	for i := range g.runners {
		g.runners[i].Draw(screen)
	}
}

// DrawResult displays the results of the run in the game window
func (g *Game) DrawResult(screen *ebiten.Image) {
	ranking := [4]int{-1, -1, -1, -1}
	for i := range g.runners {
		rank := 0
		for j := range g.runners {
			if g.runners[i].runTime > g.runners[j].runTime {
				rank++
			}
		}
		for ranking[rank] != -1 {
			rank++
		}
		ranking[rank] = i
	}

	for i := 1; i < g.resultStep && i <= 4; i++ {
		s, ms := GetSeconds(g.runners[ranking[i-1]].runTime.Milliseconds())
		ebitenutil.DebugPrintAt(screen, fmt.Sprint(i, ". P", ranking[i-1], "     ", s, ":", ms), screenWidth/2-40, 55+ranking[i-1]*20)
	}

	if g.resultStep > 4 {
		ebitenutil.DebugPrintAt(screen, "Press SPACE to restart", screenWidth/2-60, 10)
	}
}

// Draw is the main drawing function of the game. It is called by ebiten at
// each frame (60 times per second) just after calling Update (game-update.go)
// Depending of the current state of the game it calls the above utilitary
// function to draw what is needed in the game window
func (g *Game) Draw(screen *ebiten.Image) {
	screen.Fill(color.RGBA{141, 200, 235, 255})
	switch g.state {
	case StateWelcomeScreen:
		g.DrawWelcomeScreen(screen)
	case StateChooseRunner:
		g.DrawSelectScreen(screen)
	case StateLaunchRun:
		g.DrawLaunch(screen)
		g.DrawRun(screen, false)
	case StateRun:
		g.DrawRun(screen, true)
	case StateResult:
		g.DrawResult(screen)
		g.DrawRun(screen, false)
	}
}
