/*
//  Data structure for representing the four runners
//  used in the game. Provided with a few utilitary
//  methods:
//    - CheckArrival
//    - Draw
//    - DrawSelection
//    - ManualChoose
//    - ManualUpdate
//    - RandomChoose
//    - RandomUpdate
//    - Reset
//    - UpdateAnimation
//    - UpdatePos
//    - UpdateSpeed
//  Runner are editing in following methods :
//    - struct Runner
//    - NetworkUpdate
//    - UpdateSpeed
//    - UpdateSpeedOnly
//    - ChooseCheck
//    - ManualChoose
//    - NetworkChoose
//    - DrawSelection
*/

package main

import (
	"fmt"
	"image"
	"math/rand"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

type Runner struct {
	xpos, ypos          float64       // Position of the runner on the screen
	speed               float64       // Current speed of the runner
	framesSinceUpdate   int           // Number of frames since last speed update
	maxFrameInterval    int           // Maximum number of frames between two speed updates
	arrived             bool          // Tells if the runner has finished running or not
	runTime             time.Duration // Records the duration of the run for ranking
	image               *ebiten.Image // Current image used to display the runner
	colorScheme         int           // Number of the color scheme of the runner
	colorSelected       bool          // Tells if the color scheme is fixed or not
	noChangeSinceRefuse bool          // Flag represent if server denied is displayed
	animationStep       int           // Current step of the runner animation
	animationFrame      int           // Number of frames since the last animation step
	// Add bellow Property used by GameClient or updated by GameClient attached to Game contains this runner
	manualRunner   bool // Flag represent if this runner is manual runner
	colorRequested bool // Tells if the color pick are requested or not
	pickAllowed    bool // Flag represent if this pick is allowed
	index          int  // Index of this runner
	keyPressed     bool // Flag indicate if this runner are pressed a key
}

// ManualUpdate allows to use the keyboard in order to control a runner
// when the game is in the StateRun state (i.e. during a run)
func (r *Runner) ManualUpdate() {
	r.keyPressed = inpututil.IsKeyJustPressed(ebiten.KeySpace)
	r.UpdateSpeed(r.keyPressed)
	r.UpdatePos()
}

// NetworkUpdate update speed of runner if gameClient are notify
func (r *Runner) NetworkUpdate() {
	r.UpdateSpeedOnly(r.speed)
	r.UpdatePos()
}

// RandomUpdate allows to randomly control a runner when the game is in
// the StateRun state (i.e. during a run)
func (r *Runner) RandomUpdate() {
	r.UpdateSpeed(rand.Intn(3) == 0)
	r.UpdatePos()
}

// UpdateSpeed sets the speed of a runner. It is used when the game is in
// StateRun state (i.e. during a run)
func (r *Runner) UpdateSpeed(keyPressed bool) {
	// Method are unchanged in result because call of UpdateSpeedOnly
	// but structure are changed
	if keyPressed {
		r.UpdateSpeedOnly(1500 / float64(r.framesSinceUpdate*r.framesSinceUpdate*r.framesSinceUpdate))
	} else if r.framesSinceUpdate > r.maxFrameInterval {
		r.UpdateSpeedOnly(-1)
	}
}

// UpdateSpeedOnly sets the speed of runner. It is used by UpdateSpeed of UpdateNetworkSpeed
func (r *Runner) UpdateSpeedOnly(newSpeed float64) {
	// Method for update speed with specific speed, used by UpdateSpeed and NetworkUpdate
	if !r.arrived {
		r.framesSinceUpdate++
		if newSpeed != 1 {
			r.speed = newSpeed
			if r.speed > 10 {
				r.speed = 10
			}
			r.framesSinceUpdate = 0
		} else if r.framesSinceUpdate > r.maxFrameInterval {
			r.speed = 0
		}
	}
}

// UpdatePos sets the current (x) position of a runner according to the current
// speed and the previous (x) position. It is used when the game is in StateRun
// state (i.e. during a run)
func (r *Runner) UpdatePos() {
	if !r.arrived {
		r.xpos += r.speed
	}
}

// UpdateAnimation determines the next image that should be displayed for a
// runner, depending of whether or not the runner is running, the current
// animationStep and the current animationFrame
func (r *Runner) UpdateAnimation(runnerImage *ebiten.Image) {
	r.animationFrame++
	if r.speed == 0 || r.arrived {
		r.image = runnerImage.SubImage(image.Rect(0, r.colorScheme*32, 32, r.colorScheme*32+32)).(*ebiten.Image)
		r.animationFrame = 0
	} else {
		if r.animationFrame > 1 {
			r.animationStep = r.animationStep%6 + 1
			r.image = runnerImage.SubImage(image.Rect(32*r.animationStep, r.colorScheme*32, 32*r.animationStep+32, r.colorScheme*32+32)).(*ebiten.Image)
			r.animationFrame = 0
		}
	}
}

// ChooseCheck is condition for know if choose is validated
func (r Runner) ChooseCheck() (done bool) {
	// This Method is base condition required for known if pick is allowed
	// This Method are extract for call this in other method instead inside GameClient for example
	return (r.manualRunner && r.colorSelected && r.pickAllowed) || (!r.manualRunner && r.colorSelected)
}

// ManualChoose allows to use the keyboard for selecting the appearance of a
// runner when the game is in StateChooseRunner state (i.e. at player selection
// screen)
func (r *Runner) ManualChoose() (done bool) {
	// Now this Method check if player selected character,
	// update fields for notify GameClient attached to Game related to this runner
	// for request server of this changement.
	// If changement is allowed GameClient update pickAllowed to true,
	// in other case pick is cancelled
	oldColor := r.colorScheme
	pickedNow := inpututil.IsKeyJustPressed(ebiten.KeySpace)
	if !r.ChooseCheck() && pickedNow {
		r.pickAllowed = false
	}
	if !r.colorRequested {
		r.colorSelected = (!r.ChooseCheck() && pickedNow) || (r.ChooseCheck() && !pickedNow)
	}
	if pickedNow && r.colorSelected {
		r.colorRequested = true
	}
	if !r.colorSelected {
		if inpututil.IsKeyJustPressed(ebiten.KeyRight) {
			r.colorScheme = (r.colorScheme + 1) % 8
		} else if inpututil.IsKeyJustPressed(ebiten.KeyLeft) {
			r.colorScheme = (r.colorScheme + 7) % 8
		}
	}
	// This method update noChangeSinceRefuse flag for hidding last response of server
	if r.noChangeSinceRefuse && oldColor != r.colorScheme && !r.colorRequested {
		r.noChangeSinceRefuse = false
	}
	return r.ChooseCheck()
}

// NetworkChoose return state of this runner and is updated by gameClient
func (r Runner) NetworkChoose() (done bool) {
	return r.colorSelected
}

// RandomChoose allows to randomly select the appearance of a
// runner when the game is in StateChooseRunner state (i.e. at player selection
// screen)
func (r *Runner) RandomChoose() (done bool) {
	if !r.colorSelected {
		r.colorScheme = rand.Intn(8)
	}
	r.colorSelected = true
	return r.colorSelected
}

// CheckArrival allows to test if a runner has passed the arrival line
func (r *Runner) CheckArrival(f *Field) {
	if !r.arrived {
		r.arrived = r.xpos > f.xarrival
		r.runTime = time.Since(f.chrono)
	}
}

// Reset allows to reset a player state in order to be able to start a new run
func (r *Runner) Reset(f *Field) {
	r.xpos = f.xstart
	r.speed = 0
	r.framesSinceUpdate = 0
	r.arrived = false
	r.animationStep = 0
	r.animationFrame = 0
}

// Draw draws a runner on screen at the good position (defined by xpos and ypos)
func (r *Runner) Draw(screen *ebiten.Image) {
	options := &ebiten.DrawImageOptions{}
	options.GeoM.Translate(r.xpos-16, r.ypos-16)
	screen.DrawImage(r.image, options)
}

// DrawSelection draws the current selection of a runner appearance for the
// player select screen
func (r *Runner) DrawSelection(screen *ebiten.Image, xStep, playerNum int) {
	xMod := 32
	if (playerNum/2)%2 == 0 {
		xMod = -32
	}
	xPadding := (xStep + xMod) / 2
	xPos := 43 + xStep*r.colorScheme + xPadding
	yMod := 32
	if playerNum%2 == 0 {
		yMod = -62
	}
	yPos := (screenHeight + yMod) / 2
	// Adding additional Sentence for show basics informations
	// instead Confirmation, Requested of refuse of server
	additionalSentence := ""
	if r.ChooseCheck() {
		additionalSentence = "\nConfimed !"
	} else if r.colorRequested {
		additionalSentence = "\nRequested..."
	} else if r.noChangeSinceRefuse {
		additionalSentence = "\nAlready Taken."
	}
	ebitenutil.DebugPrintAt(screen, fmt.Sprint("P", playerNum, additionalSentence), xPos, yPos)
}
