// Game client created by Féry Mathieu and Mathilde Ballouhey
// GameClient represent client attached of game for exchange with server

package main

import (
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

type GameClient struct {
	hostRelated   Host        // Host related to this GameClient
	connection    net.Conn    // Connection used for exchange with server
	incommingData chan string // Chan used for receive data send by server
	outputChan    chan string // Chan used for send message with this game client
	gameRelated   *Game       // Game
	currentRunner Runner      // State of runner used for comparison for known if changed and notify only if state are changed
	endSended     bool        // Flag for indicate if end are sended
}

const (
	latencyOfCharCustomization = 750 * time.Millisecond // For prevent jump inside interface if send rate is to high
)

// Method for exchange with Server during game
func (game *Game) connectToServer(serverAddr string, serverPort int, delim rune, gameClientChan chan<- GameClient) {
	conn, err := net.Dial(defaultProtocol, fmt.Sprint(serverAddr, ":", serverPort))
	if err != nil {
		log.Fatalf(
			"Error has occured with connection to server. Please check address given. (Current address %s:%d)\nBase error : %s",
			serverAddr, serverPort, err,
		)
	}

	gameClient := GameClient{
		hostRelated: Host{
			addrRelated: conn.RemoteAddr(),
			delim:       delim,
		},
		connection:    conn,
		incommingData: make(chan string, defaultChanSize),
		outputChan:    make(chan string, defaultChanSize),
		gameRelated:   game,
	}
	gameClientChan <- gameClient
	go gameClient.hostRelated.readFrom(gameClient.connection, gameClient.incommingData)
	gameClient.manageGameExchange()
}

// Method for manage all exchange during game
func (gameClient *GameClient) manageGameExchange() {
	gameClient.syncWithServer()
	gameClient.syncWithAllPlayers()
	gameClient.waitingRunnerOrder()
	gameClient.customizationPhase()
	for {
		gameClient.countdownPhase()
		gameClient.runPhase()
	}
}

// Method for synchronize this client with server
func (gameClient *GameClient) syncWithServer() {
	var msg string
	for msg != confirmConnect {
		gameClient.hostRelated.sendTo(gameClient.connection, <-gameClient.outputChan)
		msg = <-gameClient.incommingData
		if msg != confirmConnect {
			log.Print("Wrong confirmation ", msg)
		}
	}
	log.Print("Connected successful")
}

// Method for wait for all players inside loby
func (gameClient *GameClient) syncWithAllPlayers() {
	var err error
	connected := 0
	for connected != 4 {
		select {
		case baseConnected := <-gameClient.incommingData:
			if strings.Contains(baseConnected, connectedPlayerFlag) {
				connected, err = strconv.Atoi(strings.Split(baseConnected, defaultDelimInsideMsg)[1])
				if err != nil {
					log.Print(unexpectedReturn, " : ", baseConnected)
				}
			} else {
				log.Print(unexpectedReturn, " : ", baseConnected)
			}
		}
		gameClient.gameRelated.playerNb = connected
	}
}

// Method for waiting order of own runner
func (gameClient *GameClient) waitingRunnerOrder() {
	var err error
	indexRunner := -1
	for indexRunner == -1 {
		select {
		case baseIndex := <-gameClient.incommingData:
			if strings.Contains(baseIndex, OrderFlag) {
				indexRunner, err = strconv.Atoi(strings.Split(baseIndex, defaultDelimInsideMsg)[1])
				if err != nil {
					log.Print(unexpectedReturn, " : ", baseIndex)
				}
			} else {
				log.Print(unexpectedReturn, " : ", baseIndex)
			}
		}
	}
	gameClient.gameRelated.runners[indexRunner].noChangeSinceRefuse = false
	gameClient.gameRelated.runners[indexRunner].manualRunner = true
	gameClient.gameRelated.runners[indexRunner].pickAllowed = false
	time.Sleep(time.Second)
	gameClient.gameRelated.indexRunner = indexRunner
}

// Method for manage customization phase
func (gameClient *GameClient) customizationPhase() {
	gameClient.currentRunner = gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner]
	log.Print("Start Color Selection")
	for gameClient.gameRelated.ChooseRunners() == false {
		select {
		case input := <-gameClient.incommingData:
			gameClient.processIncommingCustomization(input)
		default:
			gameClient.sendOwnCustomization()
		}
	}
}

// Method for manage network customization send by server
func (gameClient *GameClient) processIncommingCustomization(incommingData string) {
	if strings.Contains(incommingData, customizationFlag) {
		inputHash := strings.Split(incommingData, defaultDelimInsideMsg)
		indexRunnerRelated, errIndex := strconv.Atoi(inputHash[1])
		colorRelated, errColor := strconv.Atoi(inputHash[2])
		isDone, errDone := strconv.ParseBool(inputHash[3])
		if errIndex != nil || errColor != nil || errDone != nil {
			log.Print(unexpectedReturn, " : ", incommingData)
		}
		gameClient.gameRelated.runners[indexRunnerRelated].colorScheme = colorRelated
		gameClient.gameRelated.runners[indexRunnerRelated].colorSelected = isDone
	} else if incommingData == endCustomizationFlag {
		gameClient.gameRelated.allCharSelected = true
	} else if incommingData == confirmPickFlag {
		gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner].colorRequested = false
		gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner].pickAllowed = true
	} else if incommingData == dontAllowPickFlag {
		gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner].colorRequested = false
		gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner].pickAllowed = false
		gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner].noChangeSinceRefuse = true
	} else {
		log.Print(unexpectedReturn, " : ", incommingData)
	}
}

// Method for send own customization if changed and limit rate to one second
func (gameClient *GameClient) sendOwnCustomization() {
	instantRunner := gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner]
	if gameClient.currentRunner.colorScheme != instantRunner.colorScheme || gameClient.currentRunner.colorSelected != instantRunner.colorSelected {
		gameClient.hostRelated.sendTo(
			gameClient.connection,
			fmt.Sprint(
				sendServerPrefix,
				defaultDelimInsideMsg,
				gameClient.gameRelated.indexRunner,
				defaultDelimInsideMsg,
				customizationFlag,
				defaultDelimInsideMsg,
				gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner].colorSelected,
				defaultDelimInsideMsg,
				gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner].colorScheme,
			),
		)
		gameClient.currentRunner = instantRunner
	}
	time.Sleep(latencyOfCharCustomization)
}

// Method for manage countdownPhase
func (gameClient *GameClient) countdownPhase() {
	gameClient.hostRelated.sendTo(gameClient.connection, <-gameClient.outputChan)
	for gameClient.gameRelated.launchStep < 5 {
		select {
		case input := <-gameClient.incommingData:
			if strings.Contains(input, chronoFlag) {
				inputHash := strings.Split(input, defaultDelimInsideMsg)
				chronoNb, err := strconv.Atoi(inputHash[1])
				if err != nil {
					log.Print(unexpectedReturn, " : ", input)
				}
				gameClient.gameRelated.launchStep = chronoNb
			} else {
				log.Print(unexpectedReturn, " : ", input)
			}
		}
	}
}

// Method for manage run phase
func (gameClient *GameClient) runPhase() {
	log.Print("Start Run")
	gameClient.endSended = false
	for gameClient.gameRelated.CheckArrival() == false {
		select {
		case input := <-gameClient.incommingData:
			gameClient.processIncommingRunInfos(input)
		default:
			gameClient.sendOwnRunInfos()
		}
	}
}

// Method for manage network run send by server
func (gameClient *GameClient) processIncommingRunInfos(incommingData string) {
	if strings.Contains(incommingData, runFlag) {
		inputHash := strings.Split(incommingData, defaultDelimInsideMsg)
		indexRunnerRelated, errIndex := strconv.Atoi(inputHash[1])
		speedRelated, errSpeed := strconv.ParseFloat(inputHash[2], 64)
		if errIndex != nil || errSpeed != nil {
			log.Print(unexpectedReturn, " : ", incommingData)
		}
		gameClient.gameRelated.runners[indexRunnerRelated].speed = speedRelated
	} else if strings.Contains(incommingData, runEndFlag) {
		inputHash := strings.Split(incommingData, defaultDelimInsideMsg)
		indexRunnerRelated, errIndex := strconv.Atoi(inputHash[1])
		runTimeRelated, errRunTime := strconv.ParseFloat(inputHash[2], 64)
		if errIndex != nil || errRunTime != nil {
			log.Print(unexpectedReturn, " : ", incommingData)
		}
		gameClient.gameRelated.runners[indexRunnerRelated].arrived = true
		gameClient.gameRelated.runners[indexRunnerRelated].xpos = gameClient.gameRelated.f.xarrival
		gameClient.gameRelated.runners[indexRunnerRelated].runTime = time.Duration(runTimeRelated)
	} else if incommingData == timesUp {
		gameClient.gameRelated.runEnded = true
	} else {
		log.Print(unexpectedReturn, " : ", incommingData)
	}
}

// Method for send own run infos if changed
func (gameClient *GameClient) sendOwnRunInfos() {
	instantRunner := gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner]
	if instantRunner.arrived && !gameClient.endSended {
		gameClient.hostRelated.sendTo(
			gameClient.connection,
			fmt.Sprint(
				sendServerPrefix,
				defaultDelimInsideMsg,
				gameClient.gameRelated.indexRunner,
				defaultDelimInsideMsg,
				runEndFlag,
				defaultDelimInsideMsg,
				instantRunner.runTime.Nanoseconds(),
			),
		)
		gameClient.endSended = true
	} else if instantRunner.keyPressed {
		gameClient.hostRelated.sendTo(
			gameClient.connection,
			fmt.Sprint(
				sendAllExceptPrefix,
				defaultDelimInsideMsg,
				runFlag,
				defaultDelimInsideMsg,
				gameClient.gameRelated.indexRunner,
				defaultDelimInsideMsg,
				instantRunner.speed,
			),
		)
		gameClient.gameRelated.runners[gameClient.gameRelated.indexRunner].keyPressed = false
	}
}
