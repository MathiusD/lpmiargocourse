/*
// Implementation of a main function setting a few characteristics of
// the game window, creating a game, and launching it
// Main are editing in following methods :
//   - main
*/

package main

import (
	_ "image/png"
	"log"

	"github.com/hajimehoshi/ebiten/v2"
)

const (
	screenWidth  = 800 // Width of the game window (in pixels)
	screenHeight = 160 // Height of the game window (in pixels)
)

func main() {
	// Extract arguments from CLI method for base informations
	targetHost, targetPort, launchServ, startClient := processArgs()

	delim := defaultDelimMsg
	gameClientData := make(chan GameClient, defaultChanSize)

	if launchServ {
		// Launch Server if requested
		serverHost := make(chan string, defaultChanSize)
		if startClient {
			// If Server if launch with client, Server is create
			// inside goroutine and fetch host for connect client with
			// server created
			go createServer(targetPort, delim, serverHost)
			targetHost, targetPort = extractHost(<-serverHost, defaultPort)
		} else {
			// If Server if launch without client, Server is create
			// inside main thread.
			createServer(targetPort, delim, nil)
		}
	}
	if startClient {
		g := Game{}
		// Launch GameClient attached to this game
		go g.connectToServer(targetHost, targetPort, delim, gameClientData)

		ebiten.SetWindowSize(screenWidth, screenHeight)
		ebiten.SetWindowTitle("LP MiAR 2020-2021, programmation répartie")

		// Initiate game with base GameClient informations
		g = InitGame(<-gameClientData)

		// Finally launch game
		err := ebiten.RunGame(&g)
		log.Print(err)
	}
}
