// Host created by Féry Mathieu and Mathilde Ballouhey
// Host represent anyone address inside web with method for exchange with

package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
)

type Host struct {
	addrRelated net.Addr // Address related to this host
	publicAddr  net.IP   // Public address related to this host
	publicPort  int      // Public port related to this host
	delim       rune     // Delimiter used for identify each message
}

// Method for read incomming data from connection and place this data inside chan
func (host Host) readFrom(conn net.Conn, dataChan chan<- string) {
	inputReader := bufio.NewReader(conn)
	for {
		baseMsg, err := inputReader.ReadString(byte(host.delim))
		if err != nil {
			log.Print("Error in read String ", err)
		} else {
			if len(baseMsg) > 0 {
				lastIndexDelim := strings.LastIndex(baseMsg, string(host.delim))
				msg := baseMsg
				if lastIndexDelim != 1 {
					runes := []rune(msg)
					msg = string(append(runes[0:lastIndexDelim], runes[lastIndexDelim+1:]...))
				}
				log.Printf("Received \"%s\" from %s", msg, conn.RemoteAddr())
				dataChan <- msg
			}
		}
	}
}

// Method for send data inside connection and return error if occured
func (host Host) sendTo(conn net.Conn, msg string) error {
	writer := bufio.NewWriter(conn)
	_, err := writer.WriteString(fmt.Sprint(msg, string(host.delim)))
	if err != nil {
		log.Print("Error in write String ", err)
		return err
	} else {
		errFlush := writer.Flush()
		if errFlush != nil {
			log.Print("Error in Flush Data ", err)
		} else {
			log.Printf("Send %s to %s", msg, conn.RemoteAddr())
		}
		return errFlush
	}
}
