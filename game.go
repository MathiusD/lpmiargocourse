/*
//  Data structure for representing a game. Implements the ebiten.Game
//  interface (Update in game-update.go, Draw in game-draw.go, Layout
//  in game-layout.go). Provided with a few utilitary functions:
//    - initGame
//  Game are editing in following methods :
//    - struct Game
//    - InitGame
*/

package main

import (
	"bytes"
	"course/assets"
	"image"
	"log"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
)

type Game struct {
	state       int           // Current state of the game
	runnerImage *ebiten.Image // Image with all the sprites of the runners
	runners     [4]Runner     // The four runners used in the game
	f           Field         // The running field
	launchStep  int           // Current step in StateLaunchRun state
	resultStep  int           // Current step in StateResult state
	// Add bellow Property used by GameClient or updated by GameClient attached to this Game
	hostRelated       Host        // Host related to this game
	gameClientChan    chan string // Chan used for exchange with server by gameClient
	playerNb          int         // Number of player currently connected
	startingGame      bool        // Flag represent if game are started
	indexRunner       int         // Index of own runner
	allCharSelected   bool        // Flag represent if customization phase are ended
	readyForCountdown bool        // Flag represent if countdown phase if ready
	runEnded          bool        // Flag represent if server are sended end of run
}

// These constants define the five possible states of the game
const (
	StateWelcomeScreen int = iota // Title screen
	StateChooseRunner             // Player selection screen
	StateLaunchRun                // Countdown before a run
	StateRun                      // Run
	StateResult                   // Results announcement
)

// InitGame builds a new game ready for being run by ebiten
func InitGame(gameClientRelated GameClient) (g Game) {

	// Open the png image for the runners sprites
	img, _, err := image.Decode(bytes.NewReader(assets.RunnerImage))
	if err != nil {
		log.Fatal(err)
	}
	g.runnerImage = ebiten.NewImageFromImage(img)

	// Define game parameters
	start := 50.0
	finish := float64(screenWidth - 50)
	frameInterval := 20

	// Create the runners
	// Update of creation of runner for make all runner equals
	// because each runner is controlled by client now
	for i := range g.runners {
		g.runners[i] = Runner{
			xpos: start, ypos: 50 + float64(i*20),
			maxFrameInterval: frameInterval,
			colorScheme:      0,
			index:            i,
			colorSelected:    false,
		}
	}

	// Create the field
	g.f = Field{
		xstart:   start,
		xarrival: finish,
		chrono:   time.Now(),
	}

	// Define new fields of Game

	g.hostRelated = gameClientRelated.hostRelated
	g.gameClientChan = gameClientRelated.outputChan

	g.playerNb = 0
	g.indexRunner = -1
	g.allCharSelected = false
	g.launchStep = -1

	return g
}
