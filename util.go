/*
//  Implementation of a few utilitary functions:
//    - getSeconds
//  Util are editing in following methods :
//    - FindFirstString
//    - FindFirstAddr
*/

package main

import "net"

// GetSeconds splits a time in milliseconds into seconds and milliseconds
func GetSeconds(d int64) (s, ms int64) {
	s = d / 1000
	ms = d - s*1000
	return s, ms
}

// FindFirstString find first iteration of string inside array and return his index
func FindFirstString(what string, where []string) (index int) {
	for localIndex, object := range where {
		if object == what {
			return localIndex
		}
	}
	return -1
}

// FindFirstString find first iteration of net.Add inside array and return his index
func FindFirstAddr(what net.Addr, where [4]net.Addr) (index int) {
	for localIndex, object := range where {
		if object == what {
			return localIndex
		}
	}
	return -1
}
