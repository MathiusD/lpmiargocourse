// Server created by Féry Mathieu and Mathilde Ballouhey
// Server represent server manage a session with multiples clients

package main

import (
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

const (
	defaultChanSize       = 10                  // Default chan size used
	defaultProtocol       = "tcp"               // Protocol used by default in all transaction
	defaultHost           = "localhost"         // Default host used for server
	defaultPort           = 8080                // Default port used for server
	defaultDelimMsg       = '\n'                // Default delimiter for end each message
	defaultDelimInsideMsg = ":"                 // Default delimiter for seperate each instruction inside message if necessary
	requestOfConnect      = "Hello There"       // Passphrase for connect to server
	confirmConnect        = "General Kenobi"    // Response of server if connection is accepted
	connectedPlayerFlag   = "Roger"             // Flag for send number of connected player
	OrderFlag             = "Order"             // Flag for send order of player
	sendAllExceptPrefix   = "Emergency"         // Prefix to use the server as a relay to pass on information to others players
	sendServerPrefix      = "Sus"               // Prefix for send information to server
	customizationFlag     = "Colored"           // Flag for send customization informations
	confirmPickFlag       = "It's Mine"         // Flag for indicate pick is allowed
	dontAllowPickFlag     = "Go To Horny Jail"  // Flag for indicate pick isn't allowed
	endCustomizationFlag  = "Customized Ended"  // Flag for send customization phase is ended
	launchReadyFlag       = "I'm A Cow"         // Flag for send player are ready for countdown phase
	chronoFlag            = "MuffinTime"        // Flag for send time elapsed since start of countdown phase
	runFlag               = "RunBitch"          // Flag for send run speed up informations
	runEndFlag            = "23-0"              // Flag for send run end time information
	timesUp               = "It'sOver"          // Flag for send run is ended
	unexpectedReturn      = "Unexpected return" // Return unexpected
)

type Server struct {
	host                   Host          // Host related to this server
	playerAddr             [4]net.Addr   // Address related to each players
	players                [4]Player     // Players data
	listener               net.Listener  // Listener of this server
	playerChan             chan Player   // Channel for fetch player communication
	connChan               chan net.Conn // Channel for fetch incomming connection with server
	connectedPlayer        int           // Number of players connected
	customizedNb           int           // Number of players are finished customization phase
	customizedSended       bool          // If end of customization phase are sended to players
	playerReadyToCountDown int           // Number of players ready to start countdown phase
	countdownSended        bool          // If end of countdown phase are sended to players
	playersEndRun          int           // Number of players are finished run
}

// Method for create server and launch game
func createServer(port int, delim rune, hostChan chan string) {
	log.Print("Creating Serveur at ", ":", port)
	listener, err := net.Listen(defaultProtocol, fmt.Sprint(":", port))
	if err != nil {
		createServer(port+1, delim, hostChan)
	} else {
		log.Print(fmt.Sprint("Server Creating at ", listener.Addr()))

		server := Server{
			host: Host{
				addrRelated: listener.Addr(),
				delim:       delim,
				publicPort:  port,
			},
			playerAddr: [4]net.Addr{},
			players:    [4]Player{},
			listener:   listener,
			playerChan: make(chan Player, defaultChanSize),
			connChan:   make(chan net.Conn, defaultChanSize),
		}

		server.getPublicAddr()

		if hostChan != nil {
			var hostRelated string
			if server.host.publicAddr.To4() != nil {
				hostRelated = fmt.Sprint(server.host.publicAddr.To4(), ":", server.host.publicPort)
			} else if server.host.publicAddr.To16() != nil {
				hostRelated = fmt.Sprint(server.host.publicAddr.To16(), ":", server.host.publicPort)
			} else {
				hostRelated = listener.Addr().String()
			}
			hostChan <- hostRelated
		}

		server.manageIncommingPlayer()
		server.processGame()
	}
}

// Method for manage incomming Connection and wait for 4 players
func (server *Server) manageIncommingPlayer() {
	go server.Accept()
	for server.connectedPlayer < 4 {
		select {
		case player := <-server.playerChan:
			log.Printf("Player {addrRelated : %s, lastMsg : %s}", player.addrRelated, player.lastMsg)
			index := FindFirstAddr(player.addrRelated, server.playerAddr)
			if index == -1 {
				if player.lastMsg == requestOfConnect {
					log.Print("Player not found add it")
					server.playerAddr[server.connectedPlayer] = player.addrRelated
					server.players[server.connectedPlayer] = player
					server.connectedPlayer += 1
					player.outputChan <- confirmConnect
				} else {
					log.Print("Unknow Protocol, ignored.")
				}
			} else {
				log.Print("Player found update it")
				server.players[index] = player
			}
		case conn := <-server.connChan:
			go server.handleConnection(conn)
		}
		server.SendConnected()
	}
	log.Print("All Player are connected")
	server.SendPosition()
}

// Method for send number of player are connected
func (server Server) SendConnected() {
	server.SendAll(fmt.Sprint(connectedPlayerFlag, defaultDelimInsideMsg, server.connectedPlayer))
}

// Method for send position of each player
func (server Server) SendPosition() {
	log.Print("Rescue Team deployed !")
	for index, player := range server.players {
		if player.outputChan != nil {
			player.outputChan <- fmt.Sprint(OrderFlag, defaultDelimInsideMsg, index)
		}
	}
	log.Print("Rescue Team returning to camp.")
}

// Method for really execute the game
func (server *Server) processGame() {
	for {
		select {
		case incomming := <-server.playerChan:
			args := strings.Split(incomming.lastMsg, defaultDelimInsideMsg)
			if len(args) >= 2 {
				if args[0] == sendAllExceptPrefix {
					server.relayProcess(incomming)
				} else if args[0] == sendServerPrefix {
					if len(args) >= 3 {
						index, err := strconv.Atoi(args[1])
						if err != nil {
							log.Printf("Wrong index given to Server.")
						} else {
							if args[2] == customizationFlag {
								if len(args) >= 4 {
									server.customizationPhase(index, args[3], args[4])
								} else {
									log.Printf("Missing color in Customisation Instruction.")
								}
							} else if args[2] == launchReadyFlag {
								server.syncCountdownPhase(index)
							} else if args[2] == runEndFlag {
								server.endRunManagement(index, args[3])
							} else {
								log.Printf("Unknown Server Instruction in %s", incomming.lastMsg)
							}
						}
					} else {
						log.Printf("Missing Server Instruction in %s", incomming.lastMsg)
					}
				}
			} else {
				log.Printf("Unknown Instruction in %s", incomming.lastMsg)
			}
		default:
			if server.customizedNb == 4 && !server.customizedSended {
				server.endOfCustomizationPhase()
			} else if server.playerReadyToCountDown == 4 && !server.countdownSended {
				server.countdownPhase()
			} else if server.playersEndRun == 4 {
				server.endRun()
			}
		}
	}
}

// Method for update player customization state after receive message
func (server *Server) customizationPhase(writerIndex int, customizationData string, colorData string) {
	customized, errCustomized := strconv.ParseBool(customizationData)
	color, errColor := strconv.Atoi(colorData)
	if errColor != nil || errCustomized != nil {
		log.Printf("Wrong Customized of Color Value Given to Server inside : %s:%s", customizationData, colorData)
		if errCustomized == nil && customized {
			server.players[writerIndex].outputChan <- dontAllowPickFlag
		}
	} else {
		alreadyPicked := false
		if customized == true {
			for index, player := range server.players {
				if !alreadyPicked && index != writerIndex && player.color == color && player.customized {
					log.Print("This color is already picked by player ", index)
					alreadyPicked = true
					customized = false
					server.players[writerIndex].outputChan <- dontAllowPickFlag
				}
			}
		}
		oldPlayer := server.players[writerIndex]
		if customized != server.players[writerIndex].customized || color != server.players[writerIndex].color {
			server.players[writerIndex].customized = customized
			server.players[writerIndex].color = color
			if customized && !oldPlayer.customized {
				server.customizedNb += 1
				server.players[writerIndex].outputChan <- confirmPickFlag
			} else if !customized && oldPlayer.customized {
				server.customizedNb -= 1
			}
			tempWriter := server.players[writerIndex]
			tempWriter.lastMsg = fmt.Sprint(
				customizationFlag,
				defaultDelimInsideMsg,
				writerIndex,
				defaultDelimInsideMsg,
				color,
				defaultDelimInsideMsg,
				customized,
			)
			server.SendAllExceptWriter(tempWriter)
		}
	}
}

// Method for send end of customization phase to each player
func (server *Server) endOfCustomizationPhase() {
	server.SendAll("Customized Ended")
	server.customizedSended = true
}

// Method for sync all players before launch countdownphase
func (server *Server) syncCountdownPhase(writerIndex int) {
	if !server.players[writerIndex].readyToCountdown {
		server.players[writerIndex].readyToCountdown = true
		server.playerReadyToCountDown += 1
	}
}

// Method for use server instead a relay for send message of writer to other player
func (server *Server) relayProcess(writer Player) {
	writer.lastMsg = strings.ReplaceAll(writer.lastMsg, fmt.Sprint(sendAllExceptPrefix, defaultDelimInsideMsg), "")
	server.SendAllExceptWriter(writer)
}

// Method for send countdown to each player
func (server *Server) countdownPhase() {
	chrono := -1
	for chrono < 5 {
		chrono += 1
		time.Sleep(time.Second)
		server.SendAll(fmt.Sprint(chronoFlag, defaultDelimInsideMsg, chrono))
	}
	server.countdownSended = true
}

// Method for capture endRun of each player
func (server *Server) endRunManagement(writerIndex int, endRunData string) {
	var err error
	if !server.players[writerIndex].ended {
		server.players[writerIndex].ended = true
		server.players[writerIndex].endAt, err = strconv.ParseFloat(endRunData, 64)
		if err != nil {
			log.Print("Unknown duration : ", endRunData)
		} else {
			server.players[writerIndex].ended = true
			server.playersEndRun += 1
			writer := server.players[writerIndex]
			log.Print("Player ", writerIndex, " finished at ", time.Duration(writer.endAt).Seconds())
			writer.lastMsg = fmt.Sprint(
				runEndFlag,
				defaultDelimInsideMsg,
				writerIndex,
				defaultDelimInsideMsg,
				writer.endAt,
			)
			server.SendAllExceptWriter(writer)
		}
	}
}

// Method for finalize run phase
func (server *Server) endRun() {
	server.SendAll(timesUp)
	server.Reset()
}

// Method for reset for another run
func (server *Server) Reset() {
	server.countdownSended = false
	server.playerReadyToCountDown = 0
	server.playersEndRun = 0
	for index := range server.players {
		server.players[index].Reset()
	}
}
